import unittest
import main
class TestMethods(unittest.TestCase):

    def test_wall_begin(self):
        self.assertEqual(main.run([[0, 1], [1,1]], (0,0), (1,1)), -1)
        self.assertEqual(0, 0)

    def test_wall_end(self):
        self.assertEqual(main.run([[1, 1], [1,0]], (0,0), (1,1)), -1)


if __name__ == '__main__':
    unittest.main()