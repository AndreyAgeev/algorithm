WALL = -1
BLANK = -2


def create_new_lab(lab):
    for y in range(0, len(lab[0])):
        for x in range(0, len(lab)):
            if(lab[y][x] == 0):
                lab[y][x] = WALL
            else:
                lab[y][x] = BLANK
    return lab


def found(lab, begin_pos, end_pos):
    if(lab[end_pos[0]][end_pos[1]] == 0
            or lab[begin_pos[0]][begin_pos[1]] == 0):
        return -1
    lab = create_new_lab(lab)
    pos_x = begin_pos[0]
    pos_y = begin_pos[1]
    weight = 0
    dx = [1, 0, -1, 0]
    dy = [0, 1, 0, -1]
    lab[pos_x][pos_y] = 0
    while True:
        stop = True
        for y in range(0, len(lab[0])):
            for x in range(0, len(lab)):
                if(lab[y][x] == weight):
                    for k in range(0, 4):
                        iy = y + dy[k]
                        ix = x + dx[k]
                        if(iy >= 0):
                            if(iy < len(lab[0])):
                                if(ix >= 0):
                                    if(ix < len(lab)):
                                        if(lab[iy][ix] == BLANK):
                                            stop = False
                                            lab[iy][ix] = weight + 1
        weight = weight + 1
        if(stop is not False and lab[end_pos[0]][end_pos[1]] != BLANK):
            break
    if(lab[end_pos[0]][end_pos[1]] == BLANK):
        return -1
    else:
        return lab[end_pos[0]][end_pos[1]]


def run(lab, begin_pos, end_pos):
    res = found(lab, begin_pos, end_pos)
    print(res)
    return res


if __name__ == '__main__':
    lab = [
          [1, 0, 1, 0, 0],
          [1, 1, 1, 1, 0],
          [0, 0, 0, 1, 1],
          [0, 0, 1, 1, 0],
          [1, 1, 1, 1, 0],
    ]
    begin_pos = (0, 0)
    end_pos = (4, 3)
    run(lab, begin_pos, end_pos)
